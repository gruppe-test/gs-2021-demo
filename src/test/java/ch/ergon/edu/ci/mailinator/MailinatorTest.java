package ch.ergon.edu.ci.mailinator;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class MailinatorTest {

    private Mailinator mailinator = new Mailinator();

    @Tag("IntegrationTest")
    @Disabled
    @Test
    public void onlyCorrectLetters() {
        AddressEntry valid1 = new AddressEntry.Builder()
                .firstName("F1").lastName("L1").streetName("S1").houseNumber("1").plz("1111").city("Lausanne").build();
        AddressEntry valid2 = new AddressEntry.Builder()
                .firstName("F2").lastName("L2").streetName("S2").houseNumber("2").plz("2222").city("Freiburg").build();
        AddressEntry valid3 = new AddressEntry.Builder()
                .firstName("F3").lastName("L3").streetName("S3").houseNumber("3").plz("3333").city("Bern").build();
        List<AddressEntry> addressList = Arrays.asList(valid1, valid2, valid3);

        List<Letter> letters = mailinator.prepareMailing(addressList);

        Assertions.assertThat(letters).hasSize(addressList.size());
        Assertions.assertThat(letters)
                .extracting(Letter::getName)
                .anySatisfy(name -> Assertions.assertThat(name).matches("F1 L1"))
                .anySatisfy(name -> Assertions.assertThat(name).matches("F2 L2"))
                .anySatisfy(name -> Assertions.assertThat(name).matches("F3 L3"));
    }

    @Tag("IntegrationTest")
    @Disabled
    @Test
    public void incorrectLetterIsNotPrepared() {
        AddressEntry invalid1 = new AddressEntry.Builder()
                .firstName("F1").lastName("L1").streetName("S1").houseNumber("1").plz("1111").city("Nirgends").build();
        AddressEntry invalid2 = new AddressEntry.Builder()
                .firstName("F2").lastName("L2").streetName("S2").houseNumber("2").plz("22222").city("Freiburg").build();
        AddressEntry valid = new AddressEntry.Builder()
                .firstName("F3").lastName("L3").streetName("S3").houseNumber("3").plz("3333").city("Bern").build();

        List<Letter> letters = mailinator.prepareMailing(Arrays.asList(invalid1, invalid2, valid));


        Assertions.assertThat(letters).hasSize(1);
        Assertions.assertThat(letters)
                .extracting(Letter::getName)
                .noneSatisfy(name -> Assertions.assertThat(name).matches("F1 L1"))
                .noneSatisfy(name -> Assertions.assertThat(name).matches("F2 L2"))
                .anySatisfy(name -> Assertions.assertThat(name).matches("F3 L3"));

    }
}
