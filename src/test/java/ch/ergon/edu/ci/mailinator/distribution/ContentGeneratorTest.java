package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class ContentGeneratorTest {

    private static final int CONTENT_LEN = 128;
    private final ContentGenerator contentGenerator = new ContentGenerator();

    /**
     * Unit Test zu Aufgabe 2
     */
    @Disabled
    @Test
    public void letterMustContainContent() {
        Letter letter = new Letter();
        contentGenerator.insertContent(letter);
        Assertions.assertThat(letter.getContent()).isNotEmpty();
    }

    /**
     * Unit Test zu Aufgabe 2
     */
    @Disabled
    @Test
    public void contentMustHaveCorrectLength() {
        Letter letter = new Letter();
        contentGenerator.insertContent(letter);
        Assertions.assertThat(letter.getContent()).hasSize(CONTENT_LEN);
    }

}
