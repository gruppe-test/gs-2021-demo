package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;

public class AddressGenerator {

    public static final String EMPTY_SPACE = " ";

    public void insertAddress(Letter letter, AddressEntry addressEntry) {
        letter.setName(addressEntry.getFirstName() + EMPTY_SPACE + addressEntry.getLastName());
        letter.setStreet(addressEntry.getStreetName() + EMPTY_SPACE + addressEntry.getHouseNumber());
        letter.setPlz(Integer.parseInt(addressEntry.getPlz()));
        letter.setCity(addressEntry.getCity());
    }
}
